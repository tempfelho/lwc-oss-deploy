import { LightningElement } from 'lwc';

export default class ToDo extends LightningElement {
    newItemText = '';
    todo = [
        {
            name: 'Buy food'
        }
    ];

    get todoList() {
        return [...Object.keys(this.todo)].map(i => {
            this.todo[i].id = i;
            return this.todo[i];
        });
    }

    get addItemDisabled() {
        return !this.newItemText;
    }

    onNewItemChange(ev) {
        this.newItemText = ev.target.value;
    }

    onAddItem() {
        this.todo.push({name : this.newItemText});
        this.newItemText = '';
    }

    onRemoveClick(ev) {
        this.todo = this.todo.filter(el => ev.target.value != el.id);
    }
}