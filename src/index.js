import { createElement } from 'lwc';
import ToDo from 'my/toDo';

const td = createElement('to-do', { is: ToDo });
// eslint-disable-next-line @lwc/lwc/no-document-query
document.querySelector('#main').appendChild(td);
